extends Node

export var flight_length: float = 180 #seconds 

signal mission_end

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.connect("create_bullet", self, "_on_Player_Bullet_create")
	
func _on_Player_Bullet_create(from, _to, bullet_scn):
	var bullet = bullet_scn.instance()
	bullet.position = from
	add_child(bullet)
	
func _process(delta):
	flight_length -= delta
	if flight_length <= 0:
		emit_signal("mission_end")

func _autoexec_after_instance(params : Dictionary):
	self.flight_length = params.get("flight_length", 180)

extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var carboBio : RichTextLabel = $BioCargoLabel
onready var cargoChitin : Label = $ChitinCargoLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	self.carboBio.text = String(Global.get_cargo_b())
	self.cargoChitin.text = String(Global.get_cargo_c())

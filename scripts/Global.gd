extends Node

enum MISSION_TYPE {
	POINT_REACH = 0,
	CARGO_B_COLLECT = 1,
	CARGO_C_COLLECT = 2,
	DELIVER_B = 3,
	DELIVER_C = 4
}

var ship_upgrades := '{}'

var player_health : int = 100

var cargo_biomass : int = 0
var cargo_chitin  : int = 0

var active_mission

func _ready():
	pass # Replace with function body.

func get_player_health() -> int:
	return player_health
	
func chenge_health( health_delta : int):
	player_health += health_delta
	if(player_health < 0):
		player_health = 0
	if(player_health > 100):
		player_health = 100
		
	return player_health

func get_random_mission() -> int:
	var mission = floor(rand_range(0, MISSION_TYPE.size()))
	return mission
	
func drop_bio(amount):
	cargo_biomass += amount + (randi() % 3)
	
func drop_chitin(amount):
	cargo_chitin += amount + (randi() % 2)
		
func get_cargo_b():
	return self.cargo_biomass

func get_cargo_c():
	return self.cargo_chitin

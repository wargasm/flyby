extends Node2D

onready var bug = preload("res://scenes/Enemy.tscn")
onready var bugDeadParticles = preload("res://scenes/BugDead.tscn")
var Player : KinematicBody2D
var spawnReady: bool = false;
var bugSpeed: float = 170.0 

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if spawnReady == true:
		spawnReady = false
		var newBug = bug.instance();
		newBug.connect("bug_dead", self, "_on_BugDead")
		newBug.position.y = position.y
		newBug.position.x = rand_range(0, 640)
		newBug.set_speed(bugSpeed)
		add_child(newBug)
		

func _on_Spawndelay_timeout():
	spawnReady = true


func _on_Destroyer_body_entered(body):
	body.queue_free()
	pass # Replace with function body.

func _on_BugDead(bug_position, speed):
	var ptcl = bugDeadParticles.instance()
	ptcl.position = bug_position
	ptcl.speed = speed
	add_child(ptcl)
	
	


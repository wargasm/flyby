extends KinematicBody2D
var speed = 175
# Called when the node enters the scene tree for the first time.
var fire_ready: bool = true

signal create_bullet(from, to, bullet_scn)

onready var bullet = preload("res://scenes/PlayerBullet.tscn")

func _ready():
	#set_process_input(true)
	pass
	
func _physics_process(_delta: float) -> void:
	var direction := Vector2(
		Input.get_action_strength("ui_strafe_right") - Input.get_action_strength("ui_strafe_left"),
		0
	)

	direction = direction.normalized()
	var _val = move_and_slide(speed * direction)
	position.x = clamp(position.x, 15, 625)
	
func _process(_delta):
	if Input.get_action_strength("ui_fire") > 0 && fire_ready == true :
		fire_ready = false
		fire_weapon()

func _on_weaponReload_timeout():
	if(fire_ready == false):
		fire_ready = true

func fire_weapon() -> void:
	var bullet_scn = self.bullet
	emit_signal("create_bullet", position, position, bullet_scn)


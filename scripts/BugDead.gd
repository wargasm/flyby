extends Particles2D

export var speed : float
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	one_shot = true
	emitting = true
	var time = (lifetime * 2) / speed_scale
	get_tree().create_timer(time).connect("timeout", self, "queue_free")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	translate(Vector2.DOWN * delta * speed)

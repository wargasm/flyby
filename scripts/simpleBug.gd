extends KinematicBody2D

var bugSpeed
var health = 2.0
var cargo_b = 1
var cargo_c = 2
onready var Player = find_parent("Main").find_node("Player")

signal bug_dead(bug_position, speed)

# Called when the node enters the scene tree for the first time.
func _ready(): 
	pass
	
func _physics_process(delta):
	var angle = get_angle_to(Player.position)
	rotate(angle + deg2rad(90.0))
	var _val = move_and_slide(Vector2.DOWN * delta * bugSpeed * 50)

func set_speed(speed: float):
	bugSpeed = speed
	
func receive_damage(damage: float):
	$AnimationPlayer.play("hit")
	health = health - damage
	if health <= 0:
		die()
		queue_free()

func die():
	emit_signal("bug_dead", position, self.bugSpeed)
	Global.drop_bio(self.cargo_b)
	Global.drop_chitin(self.cargo_c)

extends Node

func go(sceneName, sender, params):
	var newScnName
	match sceneName:
		"Main": 
			newScnName = "res://scenes/Main.tscn"
		_:
			newScnName = "res://scenes/Start.tscn"
	print(newScnName)
	
	var new_scene : PackedScene = load(newScnName)
	 
	var ins = new_scene.instance()
	ins._autoexec_after_instance(params)
	find_parent("root").add_child(ins)
	sender.queue_free()

extends Area2D

export var bullet_speed := Vector2.UP
export var damage := 1.0



# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func _process(delta):
	var speed = bullet_speed * delta * 250
	translate(speed)
	

func _on_Timer_timeout():
	queue_free()
	

func _on_PlayerBullet_body_entered(body):
	if body.is_in_group("Destructible") and body.has_method("receive_damage"):
		body.receive_damage(damage)
	queue_free()
	pass # Replace with function body.
